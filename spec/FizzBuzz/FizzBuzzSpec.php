<?php

namespace spec\FizzBuzz;

use FizzBuzz\FizzBuzz;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FizzBuzzSpec extends ObjectBehavior
{
    function it_translate_1_for_fizzbuzz(){
        $this->excute(1)->shouldReturn(1);
    }

    function it_translate_2_for_fizzbuzz(){
        $this->excute(2)->shouldReturn(2);
    }

    function it_translate_3_for_fizzbuzz(){
        $this->excute(3)->shouldReturn('fizz');
    }

    function it_translate_5_for_fizzbuzz(){
        $this->excute(5)->shouldReturn('buzz');
    }

    function it_translate_6_for_fizzbuzz(){
        $this->excute(6)->shouldReturn('fizz');
    }

    function it_translate_10_for_fizzbuzz(){
        $this->excute(10)->shouldReturn('buzz');
    }

    function it_translate_15_for_fizzbuzz(){
        $this->excute(15)->shouldReturn('fizzbuzz');
    }

    function it_translate_51_for_fizzbuzz(){
        $this->excute(51)->shouldReturn('fizz');
    }

    function it_translate_a_sequence_of_numbers_for_fizzbuzz(){
        $this->excuteUpTo(15)->shouldReturn([1, 2, 'fizz', 4, 'buzz', 'fizz', 7, 8, 'fizz', 'buzz', 11, 'fizz', 13, 14, 'fizzbuzz']);
    }
}
