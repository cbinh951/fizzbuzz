<?php

namespace FizzBuzz;

class FizzBuzz
{
    public function excute($number){
        if($number % 15 == 0) return 'fizzbuzz';
        if($number % 3 == 0) return 'fizz';
        if($number % 5 == 0) return 'buzz';
        return $number;
    }

    public function excuteUpTo($number){
        $result = [];
        for($i = 1; $i <= $number; $i++){
            $result[] = $this->excute($i);
        }
        return $result;
    }
}
